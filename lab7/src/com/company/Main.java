package com.company;

import java.io.*;
import java.util.Scanner;
import java.util.regex.*;

public class Main {

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        String fileName;
        boolean fileExist = false;
	    while(!fileExist) {
            System.out.println("Input file name: ");
            fileName = s.nextLine();
            File file = new File(fileName);
            try(FileReader reader = new FileReader(file)){
                fileExist = true;
                char[] chars = new char[(int) file.length()];
                reader.read(chars);
                String f = new String(chars);
                reader.close();

                Pattern p = Pattern.compile("\\/\\*(.|[\\r\\n]|\\n])*\\*\\/");
                Matcher m = p.matcher(f);

                f = m.replaceAll("");

                p = Pattern.compile("\\/\\/.+");
                m = p.matcher(f);

                f = m.replaceAll("");

                try(FileWriter w = new FileWriter("output.txt")){
                    w.write(f);
                    w.flush();
                    w.close();
                } catch (IOException ex) {
                    System.out.println("Couldn't create file");
                }
            }
            catch (IOException ex) {
                System.out.println("No such file");
            }
        }
    }
}
