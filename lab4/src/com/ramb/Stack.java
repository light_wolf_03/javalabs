package com.ramb;

import java.lang.reflect.Array;
import java.util.Arrays;

public class Stack {
    public int length;
    private char[] array;
    public Stack(){
        length = 0;
        array = new char[0];
    }
    public void add(char sym){
        length += 1;
        array = Arrays.copyOf(array, length);
        array[length-1] = sym;
    }
    public char pop(){
        char tmp = array[length-1];
        array = Arrays.copyOf(array, array.length - 1);
        length -= 1;
        return tmp;
    }
}
