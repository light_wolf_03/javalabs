package com.ramb;

import java.util.*;

public class Main {

    public static void main(String[] args) {
	    Stack s = new Stack();
        Stack q = new Stack();
        MyArray m = new MyArray();
        Scanner scanner = new Scanner(System.in);

        String str = scanner.nextLine();
        for(char i: str.toCharArray()){
            s.add(i);
        }
        while (s.length != 0){
            q.add(s.pop());
        }
        while (q.length != 0){
            m.add(q.pop());
        }
        boolean error = false;

        while (m.length != 0 && !error){
            for(int i=0; i<m.length; i++){
                boolean jump = false;
                switch (m.array[i]){
                    case "]": {
                        if ((i == 0) || (!m.array[i-1].equals("["))) error = true;
                        else {
                            m.popAt(i);
                            jump = true;
                        }
                        break;
                    }
                    case ")": {
                        if ((i == 0) || (!m.array[i-1].equals("("))) error = true;
                        else {
                            m.popAt(i);
                            jump = true;
                        }
                        break;
                    }
                    case "}": {
                        if ((i == 0) || (!m.array[i-1].equals("{"))) error = true;
                        else {
                            m.popAt(i);
                            jump = true;
                        }
                        break;
                    }
                }
                if (jump) break;
            }
        }

        if(!error){
            System.out.println("Correct");
        } else {
            System.out.println("Not correct");
        }

    }
}
