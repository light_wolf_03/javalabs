package com.ramb;

import java.lang.reflect.Array;

import java.util.Arrays;
import java.util.stream.Stream;

public class MyArray {
    public int length;
    public String[] array;
    MyArray(){
        this.array = new String[0];
        this.length = 0;
    }
    void add(char c){
        this.length += 1;
        this.array = Arrays.copyOf(this.array, length);
        this.array[length-1] = "" + c;
    }
    void popAt(int at){
        String[] first = Arrays.copyOf(this.array, at - 1);
        String[] second = Arrays.copyOfRange(this.array, at + 1, this.length );
        this.array = Stream.concat(Arrays.stream(first), Arrays.stream(second)).toArray(String[]::new);
        this.length = this.array.length;
    }

    public String toString(){
        String out = "";
        for(String i: this.array){
            out += i;
        }
        return out;
    }
}
