package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	    Scanner s = new Scanner(System.in);
        int q = s.nextInt();
        switch (q){
            case 0: System.out.println("Зеро"); break;
            case 1: System.out.println("Адын"); break;
            case 2: System.out.println("Дыва"); break;
            case 3: System.out.println("Тры"); break;
            case 4: System.out.println("Читыри"); break;
            case 5: System.out.println("Пьять"); break;
            case 6: System.out.println("Шьесть"); break;
            case 7: System.out.println("Сьемь"); break;
            case 8: System.out.println("Восьемь"); break;
            case 9: System.out.println("Девьять"); break;
            default: System.out.println("Некорректное число");
        }
    }
}
