package com.company;

import java.util.Arrays;

public class Queue {
    private int length;
    private String[] array;

    public Queue(){
        length = 0;
        array = new String[0];
    }

    public void push(String sym){
        length += 1;
        array = Arrays.copyOf(array, length);
        array[length-1] = sym;
    }

    public String pop(){
        String tmp = array[0];
        this.array = Arrays.copyOfRange(array, 1, array.length);
        length -= 1;
        return tmp;
    }

    public boolean isEmpty() {
        return (this.length == 0);
    }

    public int indexOf(String s) {
        int ret = -1;
        for(int i = 0; i< length; i++) {
            if (this.array[i].equals(s)) {
                ret = i;
                break;
            }
        }
        return ret;
    }

}
