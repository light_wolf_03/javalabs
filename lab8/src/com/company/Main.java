package com.company;

public class Main {

    public static void main(String[] args) {
        Queue queue = new Queue();
        System.out.println("Queue is empty? : " + queue.isEmpty());

        queue.push("Koq");
        queue.push("Keq");
        queue.push("Kuq");
        System.out.println("Queue is empty? : " + queue.isEmpty());

        System.out.println("Index of Keq? : " + queue.indexOf("Keq"));

        System.out.println("Pop() -> : " + queue.pop());
        System.out.println("Pop() -> : " + queue.pop());
        System.out.println("Pop() -> : " + queue.pop());
        System.out.println("Queue is empty? : " + queue.isEmpty());
    }
}
