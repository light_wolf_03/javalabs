package com.ramb;

import java.util.*;
import java.io.*;
import java.util.Queue;
import java.util.stream.IntStream;

enum types {
    type1 {
        public String toString(){
            return "type1";
        }
    },
    type2 {
        public String toString(){
            return "type2";
        }
    }
}

class Wagon {
    types type;
    Integer number;

    Wagon(types t, Integer i){
        this.type = t;
        this.number = i;
    }
}

public class Main {

    public static Stack<Wagon> getStack(){
        File file = new File("banana.txt");
        Stack<Wagon> stack = new Stack<Wagon>();
        try{
            Scanner in = new Scanner(file);
            while(in.hasNext()){
                Wagon w = new Wagon(types.valueOf(in.next()), in.nextInt());
                stack.add(w);
            }
        } catch (IOException ex) {
            System.out.println(ex.toString());
        }
        return stack;
    }

    public static void printWagon(Wagon w){
        System.out.printf("Wagon<type = %s, num = %d>\n", w.type, w.number);
    }

    public static void cookFromStack(Stack<Wagon> s){
        int array_length = s.size()/2;
        Wagon[] type1 = new Wagon[array_length];
        Wagon[] type2 = new Wagon[array_length];
        int t1=0,t2=0;
        while(!s.isEmpty()){
            Wagon w=s.pop();
            if(w.type == types.type1){
                type1[t1] = w;
                t1++;
            } else {
                type2[t2] = w;
                t2++;
            }
        }
        IntStream.range(0, array_length).forEach(n -> {
            printWagon(type1[n]);
            printWagon(type2[n]);
        });
    }

    public static void main(String[] args) {
        Stack<Wagon> stack = getStack();
        cookFromStack(stack);
    }
}
