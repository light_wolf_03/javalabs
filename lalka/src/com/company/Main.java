package com.company;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Main {

    static int figure = 0;
    static char color = 'R';
    static char fill = 'N';


    public static void main(String[] args) {


        JRadioButton rb1 = new JRadioButton("Круг", true);
        JRadioButton rb2 = new JRadioButton("Квадрат");

        ButtonGroup rb = new ButtonGroup();
        rb.add(rb1);
        rb.add(rb2);

        ActionListener listener_figure = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JRadioButton btn = (JRadioButton) e.getSource();
                switch (btn.getText()){
                    case "Круг":{
                        figure = 0; break;
                    }
                    case "Квадрат": {
                        figure = 1; break;
                    }
                }
            }
        };

        rb1.addActionListener(listener_figure);
        rb2.addActionListener(listener_figure);


        JPanel rb_panel = new JPanel();
        rb_panel.add(rb1);
        rb_panel.add(rb2);




        JRadioButton rbc1 = new JRadioButton("Красный", true);
        JRadioButton rbc2 = new JRadioButton("Зеленый");
        JRadioButton rbc3 = new JRadioButton("Синий");

        ButtonGroup rbc = new ButtonGroup();
        rbc.add(rbc1);
        rbc.add(rbc2);
        rbc.add(rbc3);

        ActionListener listener_color = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JRadioButton btn = (JRadioButton) e.getSource();
                switch (btn.getText()) {
                    case "Красный": {
                        color = 'R'; break;
                    }
                    case "Зеленый": {
                        color = 'G'; break;
                    }
                    case "Синий": {
                        color = 'B'; break;
                    }
                }
            }
        };

        rbc1.addActionListener(listener_color);
        rbc2.addActionListener(listener_color);
        rbc3.addActionListener(listener_color);

        JPanel rbc_panel = new JPanel();
        rbc_panel.add(rbc1);
        rbc_panel.add(rbc2);
        rbc_panel.add(rbc3);

        JRadioButton rbz1 = new JRadioButton("Есть заливка");
        JRadioButton rbz2 = new JRadioButton("Нет заливки", true);


        ButtonGroup rbz = new ButtonGroup();
        rbz.add(rbz1);
        rbz.add(rbz2);

        ActionListener listener_fill = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JRadioButton btn = (JRadioButton) e.getSource();
                switch (btn.getText()){
                    case "Есть заливка":{
                        fill = 'Y'; break;
                    }
                    case "Нет заливки": {
                        fill = 'N'; break;
                    }
                }
            }
        };
        JPanel rbz_panel = new JPanel();
        rbz_panel.add(rbz1);
        rbz_panel.add(rbz2);

        rbz1.addActionListener(listener_fill);
        rbz2.addActionListener(listener_fill);



        ActionListener listener_button = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFrame win = new JFrame("Figure");
                win.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                win.setSize(400, 350);
                Canvas canvas = new Canvas(figure, color, fill);

                win.setLayout(new BorderLayout());
                win.add(canvas, BorderLayout.CENTER);

                win.setVisible(true);
            }
        };

        JButton btn = new JButton("Нарисовать");
        btn.addActionListener(listener_button);

        JPanel btn_panel = new JPanel();
        btn_panel.add(btn);




        JFrame w = new JFrame("Settings");
        w.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        w.setSize(400, 350);

        Container container = new Container();
        container.setLayout(new BoxLayout(container, BoxLayout.Y_AXIS));

        container.add(rb_panel);
        container.add(rbc_panel);
        container.add(rbz_panel);
        container.add(btn_panel);

        w.add(container);

//        Canvas canvas = new Canvas();
//
//        w.setLayout(new BorderLayout());
//        w.add(canvas, BorderLayout.CENTER);


        w.setVisible(true);
    }
}
