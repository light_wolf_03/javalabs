package com.ramb;

import java.util.Scanner;
import java.util.stream.IntStream;

public class Main {

    public static final int lenght = 5;

    public static void printArray(int[][] array){
        int i=0, j=0;

        System.out.print("\t");

        IntStream.range(1, 6).forEach(n -> {
            System.out.print("\t" + n);
        });

        System.out.println("\n");

        do{

            System.out.print((i+1) + "\t\t");

            do{
                System.out.print(array[i][j] + "\t");
                j++;
            } while (j < lenght);
            System.out.println();
            i++;
            j = 0;
        } while (i < lenght);
    }

    public static void main(String[] args) {
	    Scanner in = new Scanner(System.in);
        int[][] input_array = new int[lenght][lenght];

        int i = 0, j = 0;
        while (i < lenght){
            j = 0;
            while(j < lenght){
                input_array[i][j] = in.nextInt();
                j++;
            }
            i++;
        }

        printArray(input_array);

        int[][] output_array = new int[lenght][lenght];

        for (i=0; i<lenght; i++){
            for (j=0; j<lenght; j++){
                if (i<j) output_array[i][j] = input_array[i][j];
                else output_array[i][j] = 0;
            }
        }

        printArray(output_array);

    }
}
