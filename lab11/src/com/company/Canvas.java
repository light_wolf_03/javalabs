package com.company;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
public class Canvas extends JFrame {

    Container container;
    CanvasPanel canvasPanel1;
    CanvasPanel canvasPanel2;
    CanvasPanel canvasPanel3;
    CanvasPanel canvasPanel4;

    Canvas() {

        container = new Container();
        container.setLayout(new GridLayout(2, 2));

        canvasPanel1 = new CanvasPanel(Figure.Rect, false, 'R');
        canvasPanel2 = new CanvasPanel(Figure.Oval, false, 'G');
        canvasPanel3 = new CanvasPanel(Figure.Arc, false, 'B');
        canvasPanel4 = new CanvasPanel(Figure.Triangle, false, 'R');

        container.add(canvasPanel1);
        container.add(canvasPanel2);
        container.add(canvasPanel3);
        container.add(canvasPanel4);

        setContentPane(container);

        setSize(600, 600);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setVisible(true);
    }

    void set(int num, Figure fig, boolean f, char c) {
        CanvasPanel q;
        switch (num) {
            case 1: {
                q = (CanvasPanel)getContentPane().findComponentAt(1,1);
                q.set(fig, f, c);
                this.repaint();
                break;
            }
            case 2: {
                q = (CanvasPanel)getContentPane().findComponentAt(301,1);
                q.set(fig, f, c);
                this.repaint();
                break;
            }
            case 3: {
                q = (CanvasPanel)getContentPane().findComponentAt(1, 301);
                q.set(fig, f, c);
                this.repaint();
                break;
            }
            case 4: {
                q = (CanvasPanel)getContentPane().findComponentAt(301,301);
                q.set(fig, f, c);
                this.repaint();
                break;
            }
        }
    }
}
