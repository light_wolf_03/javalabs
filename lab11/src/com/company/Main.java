package com.company;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


enum Figure {
    Oval,
    Arc,
    Rect,
    Triangle
}

public class Main {

    static Canvas canvas = null;

    public static void main(String[] args) {
        ExecutorService service = Executors.newFixedThreadPool(4);

        new Settings();

        canvas = new Canvas();
    }
}