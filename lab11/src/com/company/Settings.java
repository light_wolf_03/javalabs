package com.company;

import javax.swing.*;

public class Settings extends JFrame{
    private JPanel panel1;
    private JComboBox comboBox1;
    private JCheckBox fillCheckBox;
    private JRadioButton redRadioButton;
    private JRadioButton greenRadioButton;
    private JRadioButton blueRadioButton;
    private JButton frame1Button;
    private JButton frame2Button;
    private JButton frame3Button;
    private JButton frame4Button;

    private boolean fill = false;
    private char color = 'R';

    boolean clicked1 = false;
    boolean clicked2 = false;
    boolean clicked3 = false;
    boolean clicked4 = false;


    Settings() {
        setSize(500, 350);
        setContentPane(panel1);
        setVisible(true);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        redRadioButton.addActionListener((actionEvent) -> {
            this.color = 'R';
        });

        greenRadioButton.addActionListener((actionEvent) -> {
            this.color = 'G';
        });

        blueRadioButton.addActionListener((actionEvent) -> {
            this.color = 'B';
        });

        fillCheckBox.addActionListener((actionEvent) -> {
            this.fill = !this.fill;
        });

        Timer t1 = new Timer(50, (actionEvent) -> {
            CanvasPanel g = (CanvasPanel) Main.canvas.getContentPane().findComponentAt(1, 1);
            g.repaint();
        });

        Timer t2 = new Timer(70, (actionEvent) -> {
            CanvasPanel g = (CanvasPanel) Main.canvas.getContentPane().findComponentAt(301, 1);
            g.repaint();
        });

        Timer t3 = new Timer(10, (actionEvent) -> {
            CanvasPanel g = (CanvasPanel) Main.canvas.getContentPane().findComponentAt(1, 301);
            g.repaint();
        });

        Timer t4 = new Timer(100, (actionEvent) -> {
            CanvasPanel g = (CanvasPanel) Main.canvas.getContentPane().findComponentAt(301, 301);
            g.repaint();
        });


        frame1Button.addActionListener((actionEvent) -> {
            CanvasPanel g = (CanvasPanel) Main.canvas.getContentPane().findComponentAt(1, 1);
            if (g.getColor() == this.color && g.getFill() == this.fill) {
                if (clicked1) {
                    t1.start();
                } else {
                    t1.stop();
                }
                clicked1 = !clicked1;
            } else {
                g.set(Figure.Rect, this.fill, this.color);
            }
        });

        frame2Button.addActionListener((eventAction) -> {
            CanvasPanel g = (CanvasPanel) Main.canvas.getContentPane().findComponentAt(301, 1);
            if (g.getColor() == this.color && g.getFill() == this.fill) {
                if (clicked2) {
                    t2.start();
                } else {
                    t2.stop();
                }
                clicked2 = !clicked2;
            } else {
                g.set(Figure.Oval, this.fill, this.color);
            }
        });

        frame3Button.addActionListener((actionEvent) -> {
            CanvasPanel g = (CanvasPanel) Main.canvas.getContentPane().findComponentAt(1, 301);
            if (g.getColor() == this.color && g.getFill() == this.fill) {
                if (clicked3) {
                    t3.start();
                } else {
                    t3.stop();
                }
                clicked3 = !clicked3;
            } else {
                g.set(Figure.Arc, this.fill, this.color);
            }
        });

        frame4Button.addActionListener((actionEvent) -> {
            CanvasPanel g = (CanvasPanel) Main.canvas.getContentPane().findComponentAt(301, 301);
            if (g.getColor() == this.color && g.getFill() == this.fill) {
                if (clicked4) {
                    t4.start();
                } else {
                    t4.stop();
                }
                clicked4 = !clicked4;
            } else {
                g.set(Figure.Triangle, this.fill, this.color);
            }
        });

        t4.start();

        t2.start();

        t1.start();

        t3.start();

    }
}
