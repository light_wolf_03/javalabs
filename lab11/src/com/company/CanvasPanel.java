package com.company;

import javax.swing.*;
import java.awt.*;

class CanvasPanel extends JPanel{
    private boolean fill;
    private Color color;
    private Figure figure;
    private int x4, y4=25;
    private int x3=90, y3=10;
    private int x1, y1=100;
    private int x2=0, y2=0;

    CanvasPanel(Figure fig, boolean f, char c) {
        this.figure = fig;
        this.fill = f;
        switch (c) {
            case 'R': this.color = Color.RED; break;
            case 'G': this.color = Color.GREEN; break;
            case 'B': this.color = Color.BLUE; break;
        }
    }

    public Figure getFigure() {
        return this.figure;
    }

    public boolean getFill() {
        return this.fill;
    }

    public char getColor() {
        if (this.color == Color.RED) return 'R';
        else if (this.color == Color.BLUE) return 'B';
        else if (this.color == Color.GREEN) return 'G';
        return 'R';
    }

    void set(Figure fig, boolean f, char c) {
        this.figure = fig;
        this.fill = f;
        switch (c) {
            case 'R': this.color = Color.RED; break;
            case 'G': this.color = Color.GREEN; break;
            case 'B': this.color = Color.BLUE; break;
        }
    }

    public void calcCoords2() {
        this.y2+=1;
        this.x2+=1;
        if (y2==300){y2=0;}
        if (x2==300){x2=0;}
    }
    public void calcCoords3() {
        if(this.y3==10 & this.x3<210){this.x3+=1;}
        else if(this.x3==210 & this.y3<132){this.y3+=1;}
        else if (this.y3==132 & this.x3>90){this.x3-=1;}
        else {this.y3-=1;}
    }
    public void calcCoords1() {
        this.x1+=1;
        if (x1==290){x1=0;}
    }
    private void paintOval(Graphics g, int x2, int y2) {
        g.drawOval(x2, y2, 50, 50);
    }

    private void paintArc(Graphics g, int x3, int y3) {
        int[] xs2 = {x3, x3-50, x3+80,x3-80,x3+50,x3};
        int[] ys2 = {y3, y3+148, y3+54,y3+54,y3+148,y3};
        g.drawPolygon(xs2,ys2,6);
       // g.drawArc(100, 100, 200, 200, 90, 90);
    }

    private void paintRect(Graphics g,int x1,int y1) {
        g.drawRect(x1, y1, 50, 50);
    }

    public void calcCoords4() {
        this.y4+=1;
        this.x4=(int)(Math.sin(y4)*50)+150;
        if (y4==275){y4=25;}
    }
    private void paintTriangle(Graphics g, int x4, int y4) {
        int[] xs = {x4, x4+25, x4-25};
        int[] ys = {y4-25, y4+25, y4+25};
        g.drawPolygon(xs, ys, 3);
    }

    private void clearCanvas(Graphics g) {
        g.clearRect(0, 0, getWidth(), getHeight());
    }

    private void fillCanvasWithColor(Graphics g, Color c) {
        g.setColor(c);
        g.fillRect(0, 0, getWidth(), getHeight());
    }

    public void paint(Graphics g) {
        super.paint(g);

        this.clearCanvas(g);

        if (this.fill) this.fillCanvasWithColor(g, Color.cyan);

        g.setColor(this.color);
        this.calcCoords2();
        this.calcCoords4();
        this.calcCoords1();
        this.calcCoords3();

        switch (this.figure) {
            case Arc: this.paintArc(g, this.x3, this.y3); break;
            case Oval: this.paintOval(g,this.x2, this.y2); break;
            case Rect: this.paintRect(g, this.x1, this.y1); break;
            case Triangle: this.paintTriangle(g, this.x4, this.y4); break;
        }

    }
}