package com.company;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Илья on 09.11.2016.
 */
public class MyCanvas extends JPanel {
    private int x, y;
    private int stage = 0;

    public MyCanvas(){
        this.x = 100;
        this.y = 100;
    }

    public void calcCoords() {
        if (this.stage == 0) {
            this.x += 1;
            this.y += 1;
            if (this.y == 200) this.stage = 1;
        } else if (this.stage == 1) {
            this.x += 1;
            this.y -= 1;
            if (this.y == 100) this.stage = 2;
        } else {
            this.x -= 1;
            if (this.x == 100) this.stage = 0;
        }
    }

    public void clearCanvas(Graphics g) {
        g.clearRect(0,0, getWidth(), getHeight());
    }

    public void paint(Graphics g) {
        this.clearCanvas(g);
        this.calcCoords();
        this.paintTriangle(g, this.x, this.y);
    }

    private void paintTriangle(Graphics g, int x, int y) {
        int[] xs = {x, x+25, x-25};
        int[] ys = {y-25, y+25, y+25};
        g.drawPolygon(xs, ys, 3);
    }
}
