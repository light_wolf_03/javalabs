package com.company;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Main {


    public static void main(String[] args) {
	    JFrame w = new JFrame("Koq");
        w.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        w.setSize(400, 300);


        MyCanvas canvas = new MyCanvas();

        w.setLayout(new BorderLayout());
        w.add(canvas, BorderLayout.CENTER);

        Timer t = new Timer(50, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                canvas.repaint();
            }
        });

        t.start();

        w.setVisible(true);

    }
}
