package com.company;

import java.io.*;
import java.util.Scanner;
import java.util.regex.*;
public class Main {

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        String fileName;

        boolean fileExist = false;
        while(!fileExist) {
            System.out.println("Input file name: ");
            fileName = s.nextLine();
            File file = new File(fileName);
            try(FileReader reader = new FileReader(file)){
                fileExist = true;
                char[] chars = new char[(int) file.length()];
                reader.read(chars);
                String f = new String(chars);
                reader.close();

                Pattern p = Pattern.compile("^(.+)\r\n[^.+]");
                Matcher m = p.matcher(f);

                String out = "";

                if (m.find()){
                    out = m.group().replaceAll("\r?\n|\r", "");
                    System.out.println(out);
                }

                p = Pattern.compile("\r\n\r\n(.+)\r\n\r\n");
                m = p.matcher(f);

                if (m.find()){
                    out = m.group().replaceAll("\r?\n|\r", "");
                    System.out.println(out);
                }

                p = Pattern.compile("\r\n\r\n(.+)\r\n$");
                m = p.matcher(f);

                if (m.find()){
                    out = m.group().replaceAll("\r?\n|\r", "");
                    System.out.println(out);
                }


            }
            catch (IOException ex) {
                System.out.println("No such file");
            }
        }
    }
}