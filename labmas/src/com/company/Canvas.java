package com.company;

import javax.swing.*;
import java.awt.*;


class Canvas extends JPanel{

    int figure;
    char color;
    char fill;

    Canvas(int f, char c, char i){
        this.figure = f;
        this.color = c;
        this.fill = i;
    }

    public void paintRect(Graphics g) {
        g.drawRect(150,100,100,100);

    }

    private void clearCanvas(Graphics g) {
        g.clearRect(0,0, getWidth(), getHeight());
    }

    private void paintOval(Graphics g) {
        g.drawOval(150,100,100,100);
    }

    public void paint(Graphics g) {
        this.clearCanvas(g);
        Color c = Color.black;

        switch (this.color){
            case 'R': c = Color.red; break;
            case 'G': c = Color.green; break;
            case 'B': c = Color.blue; break;
        }

        g.setColor(c);

        if (this.figure == 1) {
            this.paintRect(g);
            if(this.fill=='Y'){
                this.fillRect(g);
            }
            else {
                this.paintRect(g);
            }
        } else {
            if (this.fill == 'Y') {
                this.fillOval(g);
            } else {
                this.paintOval(g);
            }
        }
    }
    public void fillOval(Graphics g) {
        this.clearCanvas(g);
        g.fillOval(150,100,100,100);
    }
    public void fillRect(Graphics g) {
        this.clearCanvas(g);
        g.fillRect(150,100,100,100);
    }
}
