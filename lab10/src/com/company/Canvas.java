package com.company;

import javax.swing.*;
import java.awt.*;


class Canvas extends JPanel{

    int figure;
    char color;

    Canvas(int f, char c){
        this.figure = f;
        this.color = c;
    }

    private int[][] calc7Angle() {
        double x, y;
        double r = 100;
        int[][] pairs = new int[7][2];
        int j = 0;
        for(double i = 0; i < 2*Math.PI; i += (2*Math.PI / 7)) {
            x = r * Math.cos(i);
            y = r * Math.sin(i);
            pairs[j][0] = (int) x;
            pairs[j][1] = (int) y;
            j++;
        }
        return pairs;
    }

    public void paint7Angle(Graphics g) {
        this.clearCanvas(g);
        int[][] pairs = this.calc7Angle();
        int offset_x = 200, offset_y = 150;

        for (int i = 0; i < 7; i++) {
            if (i != 6) {
                g.drawLine(pairs[i][0]+offset_x, pairs[i][1]+offset_y, pairs[i+1][0]+offset_x, pairs[i+1][1]+offset_y);
            } else {
                g.drawLine(pairs[i][0]+offset_x, pairs[i][1]+offset_y, pairs[0][0]+offset_x, pairs[0][1]+offset_y);
            }
        }
    }

    private void clearCanvas(Graphics g) {
        g.clearRect(0,0, getWidth(), getHeight());
    }

    private void paintTrapeze(Graphics g) {
        g.drawLine(100, 50,  250,  50);
        g.drawLine(250, 50,  300, 250);
        g.drawLine(300, 250,  50, 250);
        g.drawLine( 50, 250, 100,  50);
    }

    public void paint(Graphics g) {
        this.clearCanvas(g);
        Color c = Color.black;

        switch (this.color){
            case 'R': c = Color.red; break;
            case 'G': c = Color.green; break;
            case 'B': c = Color.blue; break;
        }

        g.setColor(c);

        if (this.figure == 1) {
            this.paint7Angle(g);
        } else {
            this.paintTrapeze(g);
        }
    }

}
